import { Link } from 'components';

export default Home;

function Home() {
    return (
        <div>
            <h1>Simple to-do app</h1>
            <p>An example app showing how to list, add, edit and delete task records.</p>
            <p><Link href="/tasks">&gt;&gt; Manage Tasks</Link></p>
        </div>
    );
}
