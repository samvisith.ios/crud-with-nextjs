import { useState, useEffect } from 'react';

import { Link } from 'components';
import { taskService } from 'services';

export default Index;

function Index() {
    const [tasks, setTasks] = useState(null);

    useEffect(() => {
        taskService.getAll().then(x => setTasks(x));
    }, []);

    function deleteTask(id) {
        setTasks(tasks.map(x => {
            if (x.id === id) { x.isDeleting = true; }
            return x;
        }));
        taskService.delete(id).then(() => {
            setTasks(tasks => tasks.filter(x => x.id !== id));
        });
    }

    return (
        <div>
            <h1>Tasks</h1>
            <Link href="/tasks/add" className="btn btn-sm btn-success mb-2">Add Task</Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th style={{ width: '30%' }}>Task Name</th>
                        <th style={{ width: '30%' }}>Description</th>
                        <th style={{ width: '10%' }}></th>
                    </tr>
                </thead>
                <tbody>
                    {tasks && tasks.map(task =>
                        <tr key={task.id}>
                            <td> {task.name}</td>
                            <td>{task.description}</td>
                            <td style={{ whiteSpace: 'nowrap' }}>
                                <Link href={`/tasks/edit/${task.id}`} className="btn btn-sm btn-primary mr-1">Edit</Link>
                                <button onClick={() => deleteTask(task.id)} className="btn btn-sm btn-danger btn-delete-task" disabled={task.isDeleting}>
                                    {task.isDeleting 
                                        ? <span className="spinner-border spinner-border-sm"></span>
                                        : <span>Delete</span>
                                    }
                                </button>
                            </td>
                        </tr>
                    )}
                    {!tasks &&
                        <tr>
                            <td colSpan="4" className="text-center">
                                <div className="spinner-border spinner-border-lg align-center"></div>
                            </td>
                        </tr>
                    }
                    {tasks && !tasks.length &&
                        <tr>
                            <td colSpan="4" className="text-center">
                                <div className="p-2">No Tasks To Display</div>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}
