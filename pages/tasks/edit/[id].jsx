import { AddEdit } from 'components/tasks';
import { taskService } from 'services';

export default AddEdit;

export async function getServerSideProps({ params }) {
    const task = await taskService.getById(params.id);

    return {
        props: { task }
    }
}